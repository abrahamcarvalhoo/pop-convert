var visitorConfig = {
  type: 'line',
  data: {
    labels: ['Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
    datasets: [{
      fill: false,
      label: 'Visitas',
      borderColor: '#633CEA',
      backgroundColor: '#633CEA',
      data: [18,24,32,48,47,85,64],
    }, {
      fill: false,
      label: 'Conversão',
      borderColor: '#A430E9',
      backgroundColor: '#A430E9',
      data: [20,40,10,50,70,30,10],
    }]
  },
  options: {
    responsive: true,
    title: {
      display: true,
      position: 'top',
      text: 'Últimos 7 Dias',
    },
    legend: {
      position: 'bottom',
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true,
    },
    scales: {
      xAxes: [{
        display: true,
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Quantidade',
        }
      }]
    }
  }
};

var originConfig = {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [80, 40, 30],
      backgroundColor: [
        '#126ad3',
        '#f64a91',
        '#399af2',
      ],
      label: 'Dataset 1'
    }],
    labels: [
      'Facebook',
      'Instagram',
      'Google',
    ]
  },
  options: {
    responsive: true,
    legend: {
      position: 'top',
    },
    title: {
      display: false,
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  }
};

var visitor = $('#visitor');
var origin = $('#origin');

if (visitor.length) {
  window.visitorChart = new Chart(visitor, visitorConfig);
}

if (origin.length) {
  window.originChart = new Chart(origin, originConfig);
}
