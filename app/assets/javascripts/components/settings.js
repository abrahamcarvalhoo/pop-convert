$(function() {
  var wheel;

  $('.settings__devices-item').click(function() {
    var device = $(this).attr('data-id');

    $('.settings__scene').removeClass('is-desktop').removeClass('is-mobile').addClass('is-'+device);
    $(this).addClass('is-active').siblings().removeClass('is-active');
  });

  $('.settings__sidebar-item.is-active').each(function(index, value) {
    var tabActive = $(this).attr('data-id');

    checkGame();
    startGame();
    updateText();
    updateColor();
    updatePreview(tabActive);
    $('.settings__content-item#'+tabActive).addClass('is-active').siblings().removeClass('is-active');
  });

  $('.settings__sidebar-item').click(function() {
    var tabActive = $(this).attr('data-id');

    updatePreview(tabActive);
    $(this).addClass('is-active').siblings().removeClass('is-active');
    $('.settings__content-item#'+tabActive).addClass('is-active').siblings().removeClass('is-active');
  });

  $('.settings__widget input, .settings__position input, .settings__positionbar input, .settings__couponbar input, .settings__form input, .settings__form textarea').change(function() {
    updateText();
  });

  $('.settings__games input').change(function() {
    checkGame();
  });

  $('.settings__form textarea[name="recuse_message"]').focusin(function() {
    $('.popconvert__invite[data-id="recuse"]').addClass('is-active');
    $('.popconvert__invite[data-id="invite"]').removeClass('is-active');
  });

  $('.settings__form textarea[name="recuse_message"]').focusout(function() {
    $('.popconvert__invite[data-id="invite"]').addClass('is-active');
    $('.popconvert__invite[data-id="recuse"]').removeClass('is-active');
  });

  $('.settings__tab-button').click(function() {
    var index = $(this).parent().attr('data-index');

    $(this).parent().find('.settings__tab-content').slideDown();
    $(this).parent().siblings().find('.settings__tab-content').slideUp();
    $(this).parent().addClass('is-active').siblings().removeClass('is-active');
    $('.popconvert__step').siblings().removeClass('is-active').eq(index).addClass('is-active')
  });

  $('.settings__form-color input').spectrum({
    flat: false,
    showInput: true,
    allowEmpty: false,
    showButtons: true,
    preferredFormat: 'hex',
    move: function(tinycolor) {
      var name = $(this).attr('name');
      var color = tinycolor.toHexString();

      updateColorRealTime(name, color);
    },
    hide: function(tinycolor) {
      updateColor();
    }
  });

  $('.settings__form input[name="image_header"]').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      $(this).parent().parent().addClass('is-close');

      reader.onload = function(e) {
        $('.popconvert__step:first .popconvert__header').css({ 'background-image': 'url('+e.target.result+')' });
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('.settings__form-image .icon-cross').click(function() {
    $(this).parent().find('input').val('');
    $(this).parent().removeClass('is-close');
    $('.popconvert__step:first .popconvert__header').css({'background-image': 'none'});
  });

  $('.settings__params-row input[type="radio"]:checked').each(function(index, value) {
    checkForm(this);
  });

  $('.settings__params-row input[type="radio"]').change(function() {
    checkForm(this);
  });

  $('.settings__params-button#add_rewrite').click(function() {
    var rewriteItem = '<div class="settings__rewrites-item">' +
    '<select name="rewrite_type[]" required>' +
    '<option value="show">Mostrar em</option>' +
    '<option value="hide">Não mostrar</option>' +
    '</select>' +
    '<span></span>' +
    '<input type="text" name="rewrite_url[]" required>' +
    '<small class="icon-cross"></small>' +
    '</div>';

    $('.settings__rewrites').append(rewriteItem);
  });

  $('.settings__rewrites').on('click', '.settings__rewrites-item small', function() {
    $(this).parent().remove();
  });

  $('.settings__header-button#submit_settings').click(function() {
    $('.settings__content').submit();
  });
});

function checkForm(element) {
  var value = $(element).val();
  var name = $(element).attr('name');
  var inside = $(element).parents('.settings__params-item').find('.settings__params-inside input');

  if (value == 'always') {
    if (name == "show_widget") {
      $('.settings__widget input, .settings__position input').prop('disabled', false);
    }

    if (name == "show_invite") {
      $('.settings__form textarea[name="invite_message"]').prop('disabled', false);
    }

    if (name == "show_recuse") {
      $('.settings__form textarea[name="recuse_message"]').prop('disabled', false);
    }

    if (name == "show_invitebar") {
      $('.settings__form textarea[name="invitebar_message"], .settings__positionbar input').prop('disabled', false);
    }

    if (name == "show_couponbar") {
      $('.settings__couponbar input').prop('disabled', false);
    }

    inside.prop('disabled', false);
  } else {
    if (name == "show_widget") {
      $('.settings__widget input, .settings__position input').prop('disabled', true);
    }

    if (name == "show_invite") {
      $('.settings__form textarea[name="invite_message"]').prop('disabled', true);
    }

    if (name == "show_recuse") {
      $('.settings__form textarea[name="recuse_message"]').prop('disabled', true);
    }

    if (name == "show_invitebar") {
      $('.settings__form textarea[name="invitebar_message"], .settings__positionbar input').prop('disabled', true);
    }

    if (name == "show_couponbar") {
      $('.settings__couponbar input').prop('disabled', true);
    }

    inside.prop('checked', false).prop('disabled', true);
  }
}

function checkGame() {
  var game = $('.settings__games input:checked').val();

  $('.popconvert__game').removeClass('is-active');
  $('.popconvert__game#'+game).addClass('is-active');
}

function updatePreview(tab) {
  var popModal = $('.popconvert');
  var popWidget = $('.popconvert__widget');
  var popInvite = $('.popconvert__invite[data-id="invite"]');
  var popRecuse = $('.popconvert__invite[data-id="recuse"]');
  var popInviteBar = $('.popconvert__invitebar');
  var popCouponBar = $('.popconvert__couponbar');

  popModal.removeClass('is-active');
  popWidget.removeClass('is-active');
  popWidget.removeClass('is-loading');
  popInvite.removeClass('is-active');
  popRecuse.removeClass('is-active');
  popInviteBar.removeClass('is-active');
  popCouponBar.removeClass('is-active');

  if (tab == "widget") {
    popWidget.addClass('is-active');
  }

  if (tab == "invite") {
    popWidget.addClass('is-active');
    popWidget.addClass('is-loading');

    setTimeout(function() {
      var tabActive = $('.settings__sidebar-item.is-active').attr('data-id');
      if (tabActive == "invite") {
        popInvite.addClass('is-active');
        popWidget.removeClass('is-loading');
      }
    }, 3000);
  }

  if (tab == "popup") {
    popModal.addClass('is-active');
  }

  if (tab == "bar_invite") {
    popInviteBar.addClass('is-active');
  }

  if (tab == "bar_coupon") {
    popCouponBar.addClass('is-active');
  }
}

function updateText() {
  var widgetIcon = $('.settings__widget input:checked').val();
  var widgetPosition = $('.settings__position input:checked').val();
  var inviteMessage = $('.settings__form textarea[name="invite_message"]').val();
  var recuseMessage = $('.settings__form textarea[name="recuse_message"]').val();
  var primaryTitle = $('.settings__form textarea[name="primary_title"]').val();
  var primarySubTitle = $('.settings__form input[name="primary_subtitle"]').val();
  var primaryButton = $('.settings__form input[name="primary_button"]').val();
  var primaryFooter = $('.settings__form textarea[name="primary_footer"]').val();
  var secondaryTitle = $('.settings__form input[name="secondary_title"]').val();
  var secondarySubTitle = $('.settings__form input[name="secondary_subtitle"]').val();
  var secondaryButton = $('.settings__form input[name="secondary_button"]').val();
  var secondaryFooter = $('.settings__form textarea[name="secondary_footer"]').val();
  var inviteBarMessage = $('.settings__form textarea[name="invitebar_message"]').val();
  var inviteBarPosition = $('.settings__positionbar input:checked').val();
  var couponBarMessage1 = $('.settings__form input[name="couponbar_message1"]').val();
  var couponBarMessage2 = $('.settings__form input[name="couponbar_message2"]').val();
  var couponBarPosition = $('.settings__couponbar input:checked').val();
  var popStep = $('.popconvert__step');
  var popStepPrimary = $(popStep[0]);
  var popStepSecondary = $(popStep[1]);

  popStepPrimary.find('.popconvert__title').html(primaryTitle);
  popStepPrimary.find('.popconvert__subtitle').text(primarySubTitle);
  popStepPrimary.find('.popconvert__button').text(primaryButton);
  popStepPrimary.find('.popconvert__footer').text(primaryFooter);
  popStepSecondary.find('.popconvert__title strong').text(secondaryTitle);
  popStepSecondary.find('.popconvert__title small').text(secondarySubTitle);
  popStepSecondary.find('.popconvert__button').text(secondaryButton);
  popStepSecondary.find('.popconvert__footer').text(secondaryFooter);
  $('.popconvert__widget').attr('data-position', widgetPosition).attr('data-icon', widgetIcon);
  $('.popconvert__invite[data-id="invite"]').attr('data-position', widgetPosition).find('span').text(inviteMessage);
  $('.popconvert__invite[data-id="recuse"]').attr('data-position', widgetPosition).find('span').text(recuseMessage);
  $('.popconvert__invitebar').attr('data-position', inviteBarPosition).find('span').text(inviteBarMessage);
  $('.popconvert__couponbar').attr('data-position', couponBarPosition);
  $('.popconvert__couponbar-value span').text(couponBarMessage1);
  $('.popconvert__couponbar-time span').text(couponBarMessage2);
}

function updateColor() {
  var colorHeader = $('.settings__form input[name="color_header"]').val();
  var backgroundHeader = $('.settings__form input[name="background_header"]').val();
  var colorButton = $('.settings__form input[name="color_button"]').val();
  var backgroundButton = $('.settings__form input[name="background_button"]').val();
  var colorInvite = $('.settings__form input[name="color_invite"]').val();
  var backgroundInvite = $('.settings__form input[name="background_invite"]').val();
  var colorInviteBar = $('.settings__form input[name="color_invitebar"]').val();
  var backgroundInviteBar = $('.settings__form input[name="background_invitebar"]').val();
  var colorCouponBar = $('.settings__form input[name="color_couponbar"]').val();
  var colorCouponBarHight = $('.settings__form input[name="color_couponbarhight"]').val();
  var backgroundCouponBar = $('.settings__form input[name="background_couponbar"]').val();
  var colorGame = $('.settings__form input[name="color_game"]').val();
  var backgroundGame = $('.settings__form input[name="background_game"]').val();

  $('.popconvert__close').css({
    'color': colorHeader
  });

  $('.popconvert__header').css({
    'color': colorHeader,
    'background-color': backgroundHeader,
  });

  $('.popconvert__button').css({
    'color': colorButton,
    'background-color': backgroundButton,
  });

  $('.popconvert__invite').css({
    'color': colorInvite,
    'background-color': backgroundInvite,
  });

  $('.popconvert__invitebar').css({
    'color': colorInviteBar,
    'background-color': backgroundInviteBar,
  });

  $('.popconvert__couponbar').css({
    'color': colorCouponBar,
    'background-color': backgroundCouponBar,
  });

  $('.popconvert__couponbar-time strong').css({
    'color': colorCouponBarHight
  });

  $('.popconvert__reel').css({
    'border-color': backgroundGame
  });

  $('.popconvert__reel-content').css({
    'background-color': backgroundGame
  });

  $('.popconvert__reel-item div').css({
    'color': colorGame
  });

  for (var i = 1; i < wheel.segments.length; i++) {
    var backgroundGameInside = backgroundGame;

    if (i % 2 == 0) {
      backgroundGameInside = LightenColor(backgroundGameInside, 20);
    }

    wheel.segments[i].textFillStyle = colorGame;
    wheel.segments[i].fillStyle = backgroundGameInside;
    wheel.draw();
  }
}

function updateColorRealTime(name, color) {
  if (name == "color_header") {
    $('.popconvert__close').css({ 'color': colorHeader });
    $('.popconvert__header').css({ 'color': color });
  }

  if (name == "background_header") {
    $('.popconvert__header').css({ 'background-color': color });
  }

  if (name == "color_button") {
    $('.popconvert__button').css({ 'color': color });
  }

  if (name == "background_button") {
    $('.popconvert__button').css({ 'background-color': color });
  }

  if (name == "color_invite") {
    $('.popconvert__invite').css({ 'color': color });
  }

  if (name == "background_invite") {
    $('.popconvert__invite').css({ 'background-color': color });
  }

  if (name == "color_invitebar") {
    $('.popconvert__invitebar').css({ 'color': color });
  }

  if (name == "background_invitebar") {
    $('.popconvert__invitebar').css({ 'background-color': color });
  }

  if (name == "color_couponbar") {
    $('.popconvert__couponbar').css({ 'color': color });
  }

  if (name == "color_couponbarhight") {
    $('.popconvert__couponbar-time strong').css({ 'color': color });
  }

  if (name == "background_couponbar") {
    $('.popconvert__couponbar').css({ 'background-color': color });
  }

  if (name == "color_game") {
    $('.popconvert__reel-item div').css({ 'color': color });

    for (var i = 1; i < wheel.segments.length; i++) {
      wheel.segments[i].textFillStyle = color;
      wheel.draw();
    }
  }

  if (name == "background_game") {
    $('.popconvert__reel').css({ 'border-color': color });
    $('.popconvert__reel-content').css({ 'background-color': color });

    for (var i = 1; i < wheel.segments.length; i++) {
      var backgroundGameInside = color;

      if (i % 2 == 0) {
        backgroundGameInside = LightenColor(backgroundGameInside, 20);
      }

      wheel.segments[i].fillStyle = backgroundGameInside;
      wheel.draw();
    }
  }
}

function startGame(game) {
  var items = [
    {"id": "1", "name": "R$ 15 Desconto"},
    {"id": "2", "name": "10% Desconto"},
    {"id": "3", "name": "15% Desconto"},
    {"id": "4", "name": "20% Desconto"},
    {"id": "5", "name": "Frete grátis"}
  ]

  wheel = new Winwheel({
    'lineWidth': 0,
    'numSegments': 0,
    'textMargin': 15,
    'innerRadius': 80,
    'outerRadius': 220,
    'textFontSize': 19,
    'textFontWeight': 800,
    'canvasId': 'winWheel',
    'strokeStyle': '#ffffff',
    'textAlignment': 'outer',
    'textOrientation': 'curved',
    'textFontFamily': 'sans-serif',
  });

  $.each(items, function(index, value) {
    wheel.addSegment({
      'id': this.id,
      'text': this.name
    }, 1);
    wheel.draw();
  });

  wheel.addSegment({
    'id': 0,
    'text': 'Sem sorte :('
  }, 1);
  wheel.draw();

  $('.popconvert__reel-item div').each(function(index, element) {
    $.each(items, function(index, value) {
      var name = this.name.split(' ');

      if (name[2]) {
        element.innerHTML += '<small data-id="'+this.id+'"><strong>'+name[0]+' '+name[1]+'</strong> '+name[2]+'</small>';
      } else {
        element.innerHTML += '<small data-id="'+this.id+'"><strong>'+name[0]+'</strong> '+name[1]+'</small>';
      }
    });
  });
}
