$(function() {
  $('.account__avatar input').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('.account__avatar small').css({ 'background-image': 'url('+e.target.result+')' });
      }
      reader.readAsDataURL(this.files[0]);
    }
  });
});
