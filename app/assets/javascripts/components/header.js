$(function() {
  if ($(window).width() <= 992) {
    $('body, html').click(function() {
      $('.header__profile').removeClass('is-active');
    });

    $('.header__profile').click(function(e) {
      e.stopPropagation();
      $(this).toggleClass('is-active');
    });

    $('.header__nav').click(function() {
      $(this).toggleClass('is-active');
      $('.overlay').toggleClass('is-active');
      $('.header__menu').toggleClass('is-active');
      $('.header__profile').removeClass('is-active');
    });

    $('.overlay').click(function() {
      $('.overlay').removeClass('is-active');
      $('.header__nav').removeClass('is-active');
      $('.header__menu').removeClass('is-active');
      $('.header__profile').removeClass('is-active');
    });
  }
});
