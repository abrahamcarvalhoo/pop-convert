$(function() {
  $('.install__button').click(function() {
    $('.install__code').select();

    document.execCommand('copy');
    window.getSelection().removeAllRanges();

    $(this).find('span').text('Copiado!');
  });
});
