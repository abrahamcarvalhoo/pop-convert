$(function() {
  $('.coupons select[name="coupon_chance[]"]').change(function() {
    updateChance();
  });

  updateChance();
});

function updateChance() {
  var coupons = [];

  $('.coupons select[name="coupon_chance[]"]').each(function(index, value) {
    coupons.push($(this).val());
  });

  $(coupons).each(function(index, value) {
    var percent = value / (+coupons[0] + +coupons[1] + +coupons[2] + +coupons[3] + +coupons[4] + +coupons[5]) * 100;

    if (!isNaN(percent)) {
      $('.coupons tbody tr').eq(index).find('small').text(percent.toFixed(2)+'%');
    }
  });
}
